using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileGunSystem : GunSystem
{
    public MissileController missile;

    private void Start() {
        missile.gameObject.transform.localPosition = attackPoint.localPosition;
        missile.enabled = false;
        missile.collisions.enabled = false;
    }

    private void Update() {
        InputManagement();
    }

    protected override void InputManagement() {
        if (inputManager.guns.Fire.triggered) {
            Shoot();
        }

        if(!reloading && inputManager.guns.Reload.triggered) {
            Reload();
        }
    }

    protected override void Shoot() {
        missile.enabled = true;
        missile.collisions.enabled = true;
        missile.fire = true;
    }

    protected override void Reload() {
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }

    protected override void ReloadFinished() {
        if (missile.gameObject.activeInHierarchy) return;
        missile.gameObject.SetActive(true);
        missile.gameObject.transform.localPosition = attackPoint.localPosition;
        missile.fire = false;
        missile.enabled = false;
        missile.collisions.enabled = false;
    }
}
