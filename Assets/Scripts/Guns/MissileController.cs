using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

    // PUBLICS
    [Header("Reference")]
    public Rigidbody rb;
    public BoxCollider collisions;
    public ParticleSystem smokeTrailPS;
    public ParticleSystem explosionPS;
    [Header("Config")]
    public float missileSpeed = 20f;
    public float gravity = -9.8f;
    public float damage = 10f;
    public float range = 100f;
    public float gravityCooldownOnShoot = 1f;
    public bool fire = false;

    // PRIVATES
    private float _gravityTimer;
    private bool _canApplyGravity = false;
    private Vector3 _initialPosition;
    private Vector3 _velocity;
    private bool _smokeTrailIsPlaying = false;
    private bool _missileHasExploded = false;

    private void Start() {
        _gravityTimer = gravityCooldownOnShoot;
    }

    private void FixedUpdate() {

        if (fire) {

            if (!_smokeTrailIsPlaying) {
                _smokeTrailIsPlaying = true;
                smokeTrailPS.Play();
            }
            GravityCoolDownTimer();
            CalculateVelocity();
            if (_canApplyGravity) {
                ApplyGravity();
            }

            if (_missileHasExploded) {
                if (!explosionPS.isEmitting) {
                    //Destroy(gameObject);
                    gameObject.SetActive(false);
                }
            }

            rb.velocity = _velocity;

        } else {

            _initialPosition = transform.position;
            _smokeTrailIsPlaying = false;
            smokeTrailPS.Stop();

        }

        // Si la distancia de la bala supera el rango
        if (GetBulletDistanceFromShootOrigin() > range) {
            // Destruyo el objeto (Deber�a explotar)
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }


    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Player")) return;

        Vector3 collisionPoint = collision.contacts[0].point;
        ParticleSystem explosion = Instantiate(explosionPS, collisionPoint, Quaternion.identity);
        explosion.Play();
        //explosionPS.Play();
        //explosionPS.gameObject.transform.parent = null;
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }


    /// <summary>
    /// Calcula el vector _velocity del misil
    /// </summary>
    private void CalculateVelocity() {
        _velocity = transform.forward * missileSpeed;
    }

    /// <summary>
    /// Apluica al vector _velocity la gravedad 
    /// </summary>
    private void ApplyGravity() {
        _velocity.y += gravity;
    }

    /// <summary>
    /// Timer para que comience a aplicarse la gravedad y el misil
    /// tenga ca�da.
    /// </summary>
    private void GravityCoolDownTimer() {
        _gravityTimer -= Time.deltaTime;

        if (_gravityTimer <= 0) {
            _canApplyGravity = true;
        }
    }

    private float GetBulletDistanceFromShootOrigin() {
        Vector3 currentDistance = transform.position - _initialPosition;
        return currentDistance.magnitude;
    }
}
