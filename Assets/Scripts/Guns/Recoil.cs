using UnityEngine;

public class Recoil : MonoBehaviour {

    //public RecoilData recoilData;

    // Rotations
    private Vector3 _currentRotation;
    private Vector3 _targetRotation;

    // Hipfire
    [SerializeField] private float recoilX;
    [SerializeField] private float recoilY;
    [SerializeField] private float recoilZ;

    // Settings
    [SerializeField] private float snapiness;
    [SerializeField] private float returnSpeed;
 
    void Update() {
        _targetRotation = Vector3.Lerp(_targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        _currentRotation = Vector3.Slerp(_currentRotation, _targetRotation, snapiness * Time.fixedDeltaTime);
        transform.localRotation = Quaternion.Euler(_currentRotation);
    }

    public void RecoilFire() {
        _targetRotation += new Vector3(recoilX, Random.Range(-recoilY, recoilY), Random.Range(-recoilZ, recoilZ));
    }

    public void ChangeRecoilValues(RecoilData recoilData) {
        recoilX = recoilData.recoilX;
        recoilY = recoilData .recoilY;
        recoilZ = recoilData .recoilZ;
        snapiness = recoilData.snapiness;
        returnSpeed = recoilData.returnSpeed;
    }
}
