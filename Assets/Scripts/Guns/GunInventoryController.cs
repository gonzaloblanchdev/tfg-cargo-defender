using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;

/// <summary>
/// Clase encargada de gestionar el inventario de armas
/// </summary>

public class GunInventoryController : MonoBehaviour {

    [Header("Gun Inventory List")]
    public List<GameObject> guns;

    [SerializeField]
    private int _currentGunIndex = 0;

    public void UpdateGunIndex(Vector2 input)
    {
        if(guns.Count > 1)
        {
            // Desactivo el arma actual
            guns[_currentGunIndex].gameObject.SetActive(false);
            // Si la rueda del rat�n va hacia arriba
            if (input.y > 0 && guns.Count > 0){
                // Aumenta el �ndice
                if (_currentGunIndex < guns.Count - 1)
                {
                    _currentGunIndex++;
                }
                else
                {
                    _currentGunIndex = 0;
                }
            }

            // Si la rueda del rat�n va hacia abajo
            if (input.y < 0 && guns.Count > 0){
                // Disminuye el �ndice
                if (_currentGunIndex > 0)
                {
                    _currentGunIndex--;
                }
                else
                {
                    _currentGunIndex = guns.Count - 1;
                }
            }

            // Muestro el arma actual
            guns[_currentGunIndex].gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// A�ade un arma al final de la lista. M�todo que se usa en eventos (por eso tan simple)
    /// </summary>
    /// <param name="newGun"></param>
    public void AddGun(GameObject newGun){
        if(guns.Count > 0)
        {
            guns[_currentGunIndex].gameObject.SetActive(false);
        }

        guns.Add(newGun);
        _currentGunIndex = guns.Count - 1;
        newGun.GetComponent<GunSystem>().OnGunPicked();
        //guns[_currentGunIndex].GetComponent<GunControllerBase>().recoil.ChangeRecoilValues(guns[_currentGunIndex].GetComponent<GunControllerBase>().recoilData);
    }

    public void RemoveCurrentGun() {

        GunSystem gunController = guns[_currentGunIndex].GetComponent<GunSystem>();
        Debug.Log(gunController);
        gunController.ThrowGun();

        // Si hay m�s de un arma en el inventario
        if (guns.Count > 1) {

            // Destruimos el arma actual
            Destroy(guns[_currentGunIndex]);

            // Si era el primer arma de la lista
            if(_currentGunIndex == 0) {

                // Quitamos el arma de la lista
                guns.Remove(guns[_currentGunIndex]);
                // Mostramos el nuevo arma
                guns[_currentGunIndex].gameObject.SetActive(true);

                // Resto de casos
            } else {

                // Quitamos el arma de la lista
                guns.Remove(guns[_currentGunIndex]);
                // Reducimos el �ndice del arma actual
                _currentGunIndex--;
                // Mostramos el nuevo arma
                guns[_currentGunIndex].gameObject.SetActive(true);

            }
          // Si solo hay un arma
        } else {
            Destroy(guns[_currentGunIndex]);
            guns.Remove(guns[_currentGunIndex]);
            _currentGunIndex = 0;
        }

        // TODO: actualizar el �ndice para que no desaparezca el arma y se quede bloqueado el inventario
    }

}
