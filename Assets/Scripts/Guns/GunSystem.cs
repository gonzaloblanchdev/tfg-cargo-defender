using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSystem : MonoBehaviour {

    // PUBLICS
    [Header("References")]
    public Camera playerCamera;
    public Recoil recoilSystem;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LayerMask enemyLayerMask;
    [Header("Gun Stats")]
    public int bulletsPerTap;
    public float damage;
    public float timebetweenShooting;
    public float spread;
    public float range;
    public float reloadTime;
    public float timeBetweenShots;
    public int magazineSize;
    public int maxMagazines = 4;
    public bool allowButtonHold;
    [Header("Aim Configuration")]
    public Vector3 aimPosition;
    [Header("Recoil Data")]
    public RecoilData recoil;
    [Header("Particles")]
    public ParticleSystem bulletHolePS;
    public ParticleSystem bloodHitPS;
    public ParticleSystem gunMuzzlePS;
    [Header("Gun Throw Settings")]
    public GameObject gunInteractable;
    public float throwForce = 10f;

    // PRIVATES
    private int bulletsLeft;
    private int bulletsShot;
    private bool shooting;
    private bool readyToShoot;
    private bool canShoot = true;
    protected bool reloading;
    private bool _magDecremented = false;
    private bool _isAiming = false;
    private Vector3 _initialPosition;
    private int _currentMagazines;
    protected InputManager inputManager;

    private void Awake() {
        inputManager = GetComponentInParent<InputManager>();
    }

    private void Start() {
        _currentMagazines = maxMagazines;
        recoilSystem.ChangeRecoilValues(recoil);
        bulletsLeft = magazineSize;
        readyToShoot = true;
    }

    private void Update() {
        InputManagement();
    }

    /// <summary>
    /// Manages input
    /// </summary>
    protected virtual void InputManagement() {

        // If we allow button hold
        if (allowButtonHold) {
            // We check if button is being pressed
            shooting = inputManager.guns.Fire.IsPressed();
        } else {
            // Else, we check if button has being pressed once
            shooting = inputManager.guns.Fire.triggered;
        }

        // Shooting
        if (canShoot && readyToShoot && shooting && !reloading && bulletsLeft > 0) {
            bulletsShot = bulletsPerTap;
            Shoot();
        }

        // Aiming
        if (inputManager.guns.Aim.triggered) {
            AimGun();
        }

        // Reloading 
        if (inputManager.guns.Reload.triggered || bulletsLeft == 0 && !reloading) {
            Reload();
        }

    }

    /// <summary>
    /// Shoots gun
    /// </summary>
    protected virtual void Shoot() {

        readyToShoot = false;

        // Spread
        // Getting random x, y spread values
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        // Ading spread to shooting direction
        Vector3 direction = playerCamera.transform.forward + new Vector3(x, y, 0);

        // Raycast shoot
        if (Physics.Raycast(playerCamera.transform.position, direction, out rayHit, range, enemyLayerMask)) {

            Debug.Log(rayHit.collider.name);
            // Checking if ray hits an enenemy
            if (rayHit.collider.CompareTag("Enemy")) {
                rayHit.collider.GetComponent<EnemyHealth>().TakeDamage(damage);
                Instantiate(bloodHitPS, rayHit.point, Quaternion.Euler(0, 180, 0));
            } else {
                Instantiate(bulletHolePS, rayHit.point, Quaternion.Euler(0, 180, 0));
            }

        }

        // Recoil 
        recoilSystem.RecoilFire();

        // Particles 
        gunMuzzlePS.Play();

        bulletsLeft--;
        bulletsShot--;

        // Using Invoke becausa it lets me add a time to call the function
        Invoke("ResetShot", timebetweenShooting);

        if (bulletsShot > 0 && bulletsLeft > 0) {
            Invoke("Shoot", timeBetweenShots);
        }

        if (_currentMagazines == 0 && bulletsLeft == 0) {
            AudioManager.instance.PlaySound("EmptyShot");
        } else {
            AudioManager.instance.PlaySound("RifleShot");
        }
        
        GameManager.instance.UpdateAmmoHUDInfo(bulletsLeft, _currentMagazines);

    }

    public void AimGun() {
        _isAiming = !_isAiming;

        if (_isAiming) {
            transform.localPosition = aimPosition;
        } else {
            transform.localPosition = _initialPosition;
        }
    }

    public void OnGunPicked() {
        // Gun Initialization
        bulletsLeft = magazineSize;
        readyToShoot = true;
        _currentMagazines = maxMagazines;

        // References
        playerCamera = GameDataBackup.instance.playerCamera;
        recoilSystem = GameDataBackup.instance.playerRecoil;
        recoilSystem.ChangeRecoilValues(recoil);
        _initialPosition = transform.localPosition;

    }

    public void ThrowGun() {
        GameObject gun = Instantiate(gunInteractable, transform.position, transform.rotation);
        Rigidbody gunRb = gun.GetComponent<Rigidbody>();
        //gunRb.AddForce(transform.forward * throwForce);
        gunRb.velocity = GameDataBackup.instance.playerCamera.transform.forward * throwForce;
    }

    private void ResetShot() {
        readyToShoot = true;
    }

    protected virtual void Reload() {
        reloading = true;
        _magDecremented = true;
        if(_currentMagazines == 0 && bulletsLeft == 0) {
            canShoot = false;
            reloading = false;
            return;
        } else {
            if(_currentMagazines > 0) {
                _currentMagazines--;
                bulletsLeft = magazineSize;
            }
            Invoke("ReloadFinished", reloadTime);
        }

        AudioManager.instance.PlaySound("Reload");
        
    }

    protected virtual void ReloadFinished() {
        //if (_magDecremented) {
        //    _currentMagazines--;
        //    _magDecremented = false;
        //}

        GameManager.instance.UpdateAmmoHUDInfo(bulletsLeft, _currentMagazines);
        reloading = false;
    }

}
