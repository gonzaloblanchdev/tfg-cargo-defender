using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // PUBLICS
    public float damage = 10f;
    public float headDamageMultiplier = 2f;
    public float bulletVelocity = 30f;
    public float range = 100f;
    public Vector3 direction;
    public bool fire = false;
    [Header("Raycast Config")]
    public float raySize = 1f;
    public LayerMask collisionMask;

    // PRIVATES
    private Vector3 _initialPosition;


    private void FixedUpdate() {
        // Si estamos disparando, ejecuto la funci�n de movimiento
        if (fire) {
            CheckPlayerCollision();
            Move();
        } else {
            // Si no, almaceno su posici�n inicial
            _initialPosition = transform.position;
        }

        // Si la distancia de la bala supera el rango
        if (GetBulletDistanceFromShootOrigin() > range) {
            // Destruyo el objeto
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * raySize);
    }

    /// <summary>
    /// Mueve la bala en una direcci�n y con una velocidad
    /// </summary>
    private void Move() {
        transform.position += direction * bulletVelocity * Time.fixedDeltaTime;
    }

    /// <summary>
    /// Comprueba con un raycast si ha colisionado con un enemigo
    /// </summary>
    private void CheckPlayerCollision() {
        // Lanzamos el rayo
        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hitInfo, raySize, collisionMask)) {
            EnemyController enemy = hitInfo.collider.gameObject.GetComponent<EnemyController>();
            enemy.EnemyDamaged(damage);

            if(enemy != null) {
                Debug.Log("BULLET CONTROLLER: enemy refenrence set");
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("EnemyBody")) {
            EnemyController enemy = other.gameObject.GetComponentInParent<EnemyController>();
            enemy.EnemyDamaged(damage);
        }else if (other.gameObject.CompareTag("EnemyHead")) {
            EnemyController enemy = other.gameObject.GetComponentInParent<EnemyController>();
            enemy.EnemyDamaged(damage * headDamageMultiplier);
        }
    }

    /// <summary>
    /// Devuelve la distancia que hay entre la bala y el punto
    /// donde fu� disparada
    /// </summary>
    /// <returns></returns>
    private float GetBulletDistanceFromShootOrigin() {
        Vector3 currentDistance = transform.position - _initialPosition;
        return currentDistance.magnitude;
    }
}
