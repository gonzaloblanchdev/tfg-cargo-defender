using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.LowLevel.InputStateHistory;

public class AssaultRifleController : GunControllerBase {

    [SerializeField]
    private InputManager inputManager;

    void Start() {
        inputManager = GetComponentInParent<InputManager>();
        recoil = GetComponentInParent<Recoil>();
        RecoilFunction.AddListener(recoil.RecoilFire);
        ConvertFireRateToSeconds();
        gunInitialPosition = transform.localPosition;
        currentAmmo = magCapacity;
        currentMags = totalMags;
    }

    void Update() {
        recoil.ChangeRecoilValues(recoilData);

        if (enableTimer) {
            RateOfFireTimer();
        }

        if (canShoot && inputManager.guns.Fire.IsPressed()) {
            BaseShoot();
            muzzleSystem.Play();
        }

        if (inputManager.guns.Aim.triggered) {
            aimGun = !aimGun;
            AimGun();
        }
    }
}
