using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;

/// <summary>
/// Script base del funcionamiento de las armas. Todas las armas disparan llamando
/// a la misma funci�n. Dentro del script de cada arma, heredado de este, a trav�s
/// del input, se llamar� de una manera u otra simulando disparo autom�tico o 
/// semiautom�tico.
/// </summary>

[System.Serializable]
public abstract class GunControllerBase : MonoBehaviour {

    public UnityEvent RecoilFunction;
    public UnityEvent<float[]> OnGunEnable;
   

    // PUBLICS
    [Header("References")]
    public GameObject bulletPrefab;
    public Transform shootOrigin;
    public ParticleSystem muzzleSystem;
    public AudioSource gunAudioSource;
    public AudioClip gunSound;
    public RecoilData recoilData;
    public GameObject throwableGunInteractable; // Interactable del arma que se lanza cuando se tira el arma
    public Transform gunThrowParent;
    [HideInInspector]
    public Recoil recoil;
    [Header("Shoot Config")]
    public Vector3 aimPosition;
    public float aimTime        = 1f;
    public float shotsPerMinute = 80f;
    public float damage         = 10f;
    public float range          = 100f;
    public int magCapacity      = 31;
    public int totalMags        = 4;
    public float gunThrowForce  = 2;
    public UnityEvent<int,int> OnShoot;

    // PRIVATES & PROTECTED
    [SerializeField]
    protected float secsBetweenShots;
    protected bool canShoot     = true;
    [SerializeField]
    private float rateOfFireTimer;
    protected bool enableTimer  = false;
    protected Vector3 gunInitialPosition;
    private float gunAimTimer;
    protected bool aimGun       = false;
    protected int currentAmmo;
    protected int currentMags;

    public bool gunCanShoot {
        get { return canShoot; }
        set { canShoot = true; }
    }


    //private void OnEnable() {
    //    recoil.ChangeRecoilValues(recoilData);
    //}

        /// <summary>
        /// Funci�n que gestiona el disparo del arma y el resto de sistemas 
        /// que intervienen en el disparo. Disparo, munici�n, recoil...
        /// </summary>
    public virtual void BaseShoot() {
        if (!canShoot) return;
        // Disparamos y ejecutamos la funci�n de recoil
        Shoot();
        RecoilFunction.Invoke();
        // Gestionamos la munici�n
        currentAmmo--;
        if(currentAmmo == 0) {
            currentMags--;
            if(currentMags > 0) {
                currentAmmo = magCapacity;
            }
        }
        // Si estamos sin munici�n
        if(currentMags == 0) {
            // Desactivamos el disparo
            currentAmmo = 0;
            DisableShooting();
        }
        // Actualizamos los valores del HUD con el evento
        OnShoot.Invoke(currentAmmo, currentMags);
    }

    /// <summary>
    /// Funci�n que permite apuntar el arma
    /// </summary>
    protected void AimGun() {
        gunAimTimer += Time.deltaTime;
        float t = gunAimTimer / aimTime;

        if (aimGun) {
            //transform.position = Vector3.Lerp(gunInitialPosition, aimPosition, t);
            transform.localPosition = aimPosition;
        } else {
            transform.localPosition = gunInitialPosition;
        }

        if(t > 1) {
            gunAimTimer = 0;
        }
    }

    /// <summary>
    /// Funci�n que dispara una bala. Esta funci�n se encarga solo del disparo
    /// </summary>
    private void Shoot() {
        GameObject bullet = Instantiate(bulletPrefab, shootOrigin.position, shootOrigin.rotation);
        BulletController bulletController = bullet.GetComponent<BulletController>();
        bulletController.direction = shootOrigin.forward;
        bulletController.fire = true;
        canShoot = false;
        enableTimer = true;
        muzzleSystem.Play();
        //TODO: que los audios suenen de audiosources
        //gunAudioSource.PlayOneShot(gunSound);
        AudioManager.instance.StopSound("RifleShot");
        AudioManager.instance.PlaySound("RifleShot");
    }

    /// <summary>
    /// Funci�n que desactiva el disparo
    /// </summary>
    private void DisableShooting() {
        enableTimer = false;
        canShoot = false;
    }

    /// <summary>
    /// Funci�n que convierte el ROF en segundos que hay entre
    /// disparo y disparo, para usar el timer.
    /// </summary>
    protected void ConvertFireRateToSeconds() {
        secsBetweenShots = 60f / shotsPerMinute;
    }

    /// <summary>
    /// Timer que se ejecuta entre disparo y disparo
    /// </summary>
    protected void RateOfFireTimer() {
        rateOfFireTimer += Time.deltaTime;
        if(rateOfFireTimer >= secsBetweenShots ) {
            canShoot = true;
            rateOfFireTimer = 0;
            enableTimer = false;
        }
    }

    public void ThrowGun() {
        GameObject gun = Instantiate(throwableGunInteractable, transform.position, transform.rotation);
        Rigidbody gunRb = gun.GetComponent<Rigidbody>();
        gunRb.AddForce(transform.forward * gunThrowForce);
    }
}
