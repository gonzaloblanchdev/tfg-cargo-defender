using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.LowLevel.InputStateHistory;

public class PistolController : GunControllerBase {

    private InputManager inputManager;
    

    void Start() {
        inputManager = GetComponentInParent<InputManager>();
        ConvertFireRateToSeconds();
        recoil = GetComponentInParent<Recoil>();
        RecoilFunction.AddListener(recoil.RecoilFire);
        gunInitialPosition = transform.localPosition;
        currentAmmo = magCapacity;
        currentMags = totalMags;
        recoil.ChangeRecoilValues(recoilData);
    }

    void Update() {
        recoil.ChangeRecoilValues(recoilData);

        if (enableTimer) {
            RateOfFireTimer();
        }

        if (inputManager.guns.Fire.triggered && canShoot) {
            BaseShoot();
            muzzleSystem.Play();
        }

        if (inputManager.guns.Aim.triggered) {
            aimGun = !aimGun;
            AimGun();
        }
    }

}
