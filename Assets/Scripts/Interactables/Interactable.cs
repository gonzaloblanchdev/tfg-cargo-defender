using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase base d�nde se define la informaci�n com�n de los objetos
/// interactables. Todos los objetos con interacci�n
/// </summary>

// Uso una abstract class por que este script est� destinado 
// a ser una clase base de las que heredan todos los interactables

public abstract class Interactable : MonoBehaviour {

    // Mensaje que se muestra al jugador cuando mira un interactable
    public string promptMessage;

    /// <summary>
    /// Funci�n donde se realiza todo lo com�n a la interacci�n y
    /// se llama al metodo Interact de cada interactable
    /// </summary>
    public void BaseInteract() {
        Interact();
    }

    /// <summary>
    /// Funci�n base destinada a ser sobrescrita por 
    /// los hijos de esta clase (virtual para que 
    /// pueda ser sobrescrita)
    /// </summary>
    protected virtual void Interact()
    {

    } 
}
