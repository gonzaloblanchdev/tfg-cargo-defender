using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class GunPickable : Interactable
{
    [Header("Reference")]
    public GameObject gunPrefab;
    public Vector3 localPosition;

    protected override void Interact(){
        // Instanciamos el arma
        GameObject gun = Instantiate(gunPrefab, GameDataBackup.instance.gunsParent);
        // Le asignamos su posici�n local
        gun.transform.localPosition = localPosition;
        // A�adimos el arma en el inventario
        GameDataBackup.instance.inventoryController.AddGun(gun);
        Destroy(gameObject);
    }
}
