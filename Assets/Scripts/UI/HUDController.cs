using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUDController : MonoBehaviour {

    [Header("HUD Elements")]
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI magazinesText;
    public TextMeshProUGUI roundText;

    public void UpdateAmmoText(int ammo) {
        ammoText.text = ammo.ToString();
    }
    
    public void UpdateMagazineText(int mag) {
        magazinesText.text = mag.ToString();
    }

    public void UpdateRoundText(int round) {
        roundText.text = "Round: " + round.ToString();
    }
}
