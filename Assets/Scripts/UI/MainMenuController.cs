using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    // PUBLICS
    [Header("Menu Tabs")]
    public GameObject welcomeMenu;
    public GameObject menu;
    public GameObject creditsMenu;
    public GameObject optionsMenu;
    // INPUT ACTIONS
    public MenuInput.UIActions ui;
    [Header("Camera Animator")]
    public Animator animator;

    // PRIVATES
    private MenuInput menuInput;
    private GameObject _currentTab;

    private void Start(){
        _currentTab = welcomeMenu;
        _currentTab.SetActive(true);
    }

    private void Awake() {
        menuInput = new MenuInput();
        ui = menuInput.UI;
    }

    public void PlayGame() {
        SceneManager.LoadScene("Game");
    }

    public void GoToMenu() {
        animator.SetTrigger("ToMainMenu");
        _currentTab.SetActive(false);
        menu.SetActive(true);
        _currentTab = menu;
    }

    public void GoToCreditsMenu()
    {
        animator.SetTrigger("ToCreditsMenu");
        _currentTab.SetActive(false);
        creditsMenu.SetActive(true);
        _currentTab = creditsMenu;
    }

    public void GoToOptionsMenu()
    {
        animator.SetTrigger("ToOptionsMenu");
        _currentTab.SetActive(false);
        optionsMenu.SetActive(true);
        _currentTab = optionsMenu; 
    }

    public void QuitGame()
    {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #endif
        Application.Quit();
    }
}
