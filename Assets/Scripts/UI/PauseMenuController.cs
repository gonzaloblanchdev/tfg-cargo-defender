using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour {

    [Header("References")]
    public PlayerLook look;
    public Slider sensitivitySlider;

    public void UpdateSensityvity() {
        look.SetLookSensitivity(sensitivitySlider.value);
    }
}
