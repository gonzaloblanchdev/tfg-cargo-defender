using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWavesManager : MonoBehaviour
{
    public int maxRounds = 10;
    public int enemiesPerRound = 6;
    public bool canSpawnBoat = true;

    private int _enemiesKilled = 0;
    private int _enemiesSpawned = 0;
    private int _currentWave = 1;

    private void Update() {
        canSpawnBoat = _enemiesSpawned < enemiesPerRound;
    }

    public void OnEnemySpawned() {
        _enemiesSpawned++;
    }

    public void OnEnemyKilled() {
        _enemiesKilled++;

        if(_enemiesKilled >= enemiesPerRound) {
            OnRoundFinished();
        }
    }

    public void OnRoundFinished() {
        _currentWave++;
        if(_currentWave > maxRounds) {
            // TODO: Gana el juego (PROVISIONAL)
            Debug.Log("Ganas!");
        } else {
            _enemiesKilled = 0;
            _enemiesSpawned = 0;
            GameManager.instance.playerHUD.UpdateRoundText(_currentWave);
        }
    }
}
