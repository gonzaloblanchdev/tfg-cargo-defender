using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataBackup : MonoBehaviour
{
    [Header("PLAYER")]
    public Transform playerTransform;
    public Camera playerCamera;
    public Recoil playerRecoil;
    [Header("GUNS")]
    public Transform gunsParent;
    public GunInventoryController inventoryController;
    [Header("Boat Points")]
    public Transform[] dropPoints;
    public Transform[] patrolPoints;

    public static GameDataBackup instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(this);
        }
    }
}
