using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Recoil recoil;
    public CargoEnemySpawner cargoSpawner;
    public HUDController playerHUD;
    public EnemyWavesManager wavesManager;

    public static GameManager instance;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(this);
        }
    }

    private void Start() {
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void PauseGame() {
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ResumeGame() {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    /// <summary>
    /// Actualiza los valores de retroceso
    /// </summary>
    /// <param name="newRecoil"></param>
    public void SetNewRecoil(RecoilData newRecoil) {
        recoil.ChangeRecoilValues(newRecoil);
    }

    /// <summary>
    /// Ejecuta retroceso del arma
    /// </summary>
    public void OnGunShoot() {
        
    }

    public void LoadEnemiesInCargoSpawner(GameObject[] enemies) {
        cargoSpawner.enemiesToSpawn = enemies;
        cargoSpawner.ResetSpawner();
    }

    // HUD
    public void UpdateAmmoHUDInfo(int ammo, int mag) {
        playerHUD.UpdateAmmoText(ammo);
        playerHUD.UpdateMagazineText(mag);
    }
}
