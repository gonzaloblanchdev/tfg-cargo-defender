using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase encargada del movimiento del minimapa
/// </summary>

public class MiniMap : MonoBehaviour {

    public Transform player;

    private void LateUpdate() {
        // Actualizamos la posici�n
        Vector3 newPosition = player.position;
        newPosition.y = transform.position.y;
        transform.position = newPosition;

        // A�adimos la misma rotaci�n en y que el player
        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }

}
