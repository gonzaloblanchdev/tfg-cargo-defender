using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticTurretController : MonoBehaviour
{
    public Transform turretGun;
    public float enemyDetectionRadius = 20f;
    public float rotationSpeed = 1f;

    private IAEnemyController _target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScanForEnemies();
        AimForTarget();
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, enemyDetectionRadius);
    }

    public void AimForTarget() {
        if (_target == null) return;
        //turretGun.transform.LookAt(_target.gameObject.transform);
        Quaternion targetRotation = Quaternion.LookRotation(_target.gameObject.transform.position - turretGun.transform.position);
        turretGun.transform.rotation = Quaternion.Slerp(turretGun.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    public void ScanForEnemies() {
        Collider[] collisions = Physics.OverlapSphere(transform.position, enemyDetectionRadius);
        foreach (Collider collision in collisions) {
            if (collision.gameObject.CompareTag("Enemy")) {
                _target = collision.gameObject.GetComponent<IAEnemyController>();
            }
        }

        if(collisions.Length == 0) {
            _target = null;
        }
    }

}
