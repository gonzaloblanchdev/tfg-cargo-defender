using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RAutomaticGunController : RGunControllerBase
{
    private void FixedUpdate() {
        if (inputManager.guns.Fire.IsPressed()) {
            Shoot();
            gunHasShot = true;
            canShoot = false;
        }

        if (gunHasShot) {
            RateOfFireTimer();
        }
    }
}
