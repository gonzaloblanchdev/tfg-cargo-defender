using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGunControllerBase : MonoBehaviour
{
    [Header("Reference")]
    public Transform shootOrigin;
    public RecoilData recoilData;
    [Header("Gun Configuration")]
    public Vector3 aimPosition;
    public float shotsPerMinute = 80f;
    public float damage = 10f;
    public float range = 100f;

    [SerializeField]
    protected float secsBetweenShots;
    [SerializeField]
    protected bool canShoot = true;
    [SerializeField]
    protected bool gunHasShot = false;
    protected InputManager inputManager;
    [SerializeField]
    private float _rofTimer;

    private void OnEnable() {
        GameManager.instance.SetNewRecoil(recoilData);
    }

    private void Awake() {
        // TODO: Gestionar asignación del input manager cuando se coge arma del suelo
        inputManager = GetComponentInParent<InputManager>();
    }

    private void Start() {
        ConvertFireRateToSeconds();
    }

    public void Shoot() {
        if (!canShoot) return;
        GameManager.instance.OnGunShoot();
        if (Physics.Raycast(shootOrigin.position, GameDataBackup.instance.playerCamera.transform.forward, out RaycastHit hitInfo, range)) {
            if(hitInfo.collider.CompareTag("EnemyHead") || hitInfo.collider.CompareTag("EnemyBody")) {
                Debug.Log("ENEMY HIT");
            }
            Debug.DrawRay(GameDataBackup.instance.playerCamera.transform.position, GameDataBackup.instance.playerCamera.transform.forward * range);
        }
    }

    protected void ConvertFireRateToSeconds() {
        secsBetweenShots = 60f / shotsPerMinute;
    }

    protected void RateOfFireTimer() {
        _rofTimer += Time.deltaTime;
        if(_rofTimer >= secsBetweenShots) {
            canShoot = true;
            _rofTimer = 0;
            gunHasShot = false;
        }
    }
}
