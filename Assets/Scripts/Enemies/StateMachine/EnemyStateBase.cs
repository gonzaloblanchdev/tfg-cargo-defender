using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyStateBase : StateBase
{
    public StateMachineController stateMachine;
    public IAEnemyController enemyController;

    public abstract override void StateEnter();

    public abstract override void StateExit();

    public abstract override void StateInput();

    public abstract override void StateLateStep();

    public abstract override void StatePhysicsStep();

    public abstract override void StateStep();
}
