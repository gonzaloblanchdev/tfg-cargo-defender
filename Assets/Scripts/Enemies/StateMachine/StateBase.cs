using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateBase : MonoBehaviour {

    [Tooltip("Nombre del estado")]
    public string stateName;

    [Tooltip("Maquina de estados a la que pertence este estado")]
    StateMachineController stateMachine;

    public abstract void StateEnter();
    public abstract void StateExit();
    public abstract void StateInput();
    public abstract void StateStep();
    public abstract void StatePhysicsStep();
    public abstract void StateLateStep();
}
