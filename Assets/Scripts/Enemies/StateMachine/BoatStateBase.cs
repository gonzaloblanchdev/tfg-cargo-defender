using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatStateBase : StateBase
{
    [Header("BoatStateBase")]
    public IABoatController boatController;
    public StateMachineController stateMachine;

    public override void StateEnter() {
        
    }

    public override void StateExit() {
        
    }

    public override void StateInput() {
        
    }

    public override void StateLateStep() {
        
    }

    public override void StatePhysicsStep() {
        
    }

    public override void StateStep() {
        
    }

}
