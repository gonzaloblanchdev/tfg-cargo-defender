using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineController : MonoBehaviour {


    public StateBase[] states;

    [Header("DEBUG")]
    [SerializeField]
    private StateBase _currentState;

    public void Initialize() {
        if (states != null && states.Length > 0) {
            SetState(states[0].stateName);
        }
    }

    public void Step() {
        if(_currentState != null) {
            _currentState.StateInput();
            _currentState.StateStep();
        }
    }

    public void PhysicsStep() {
        if(_currentState != null) {
            _currentState.StatePhysicsStep();
        }
    }

    public void LateStep() {
        if(_currentState != null) { }
        _currentState.StateLateStep();
    }

    public void SetState(string stateName) {
        // Obtenemos referencia al pr�ximo estado
        StateBase nextState = GetStateWithName(stateName);
        // Si no existe no hacemos nada
        if(nextState == null) {
            return;
        }
        // Si hay estado actual activo
        if(_currentState != null) {
            _currentState.StateExit();
        }
        // Actualizamos al nuevo estado
        _currentState = nextState;
        // Ejecutamos c�digo de entrada
        _currentState.StateEnter();
        
    }

    /// <summary>
    /// Devuelve el estado cuyo nombre coincide con el indicado por par�metro
    /// </summary>
    /// <param name="stateName">Nombre del estado</param>
    /// <returns></returns>
    private StateBase GetStateWithName(string stateName) {
        for (int i = 0; i < states.Length; i++) {
            if (states[i].stateName == stateName) {
                return states[i];
            }
        }

        return null;
    }
}
