using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAEnemyController : MonoBehaviour {

    [Header("References")]
    public IAEnemyMovement movement;
    public IAGunSystem gun;
    public StateMachineController stateMachine;

    void Start() {
        stateMachine.Initialize();
    }

    void Update() {
        stateMachine.Step();
    }

    private void FixedUpdate() {
        stateMachine.PhysicsStep();
    }

    private void LateUpdate() {
        stateMachine.LateStep();
    }

    public void InitializeEnemy() {
        movement.target = GameDataBackup.instance.playerTransform;
    }
}
