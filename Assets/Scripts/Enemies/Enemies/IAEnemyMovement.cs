using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAEnemyMovement : MonoBehaviour {

    // PUBLICS
    [Header("References")]
    public Transform target;
    [Header("Movement Configuration")]
    public float minDistanceToPlayer = 10f;
    public float lookRotationSpeed = 15f;

    private NavMeshAgent _agent;
    private bool _canMove = true;
    private bool _isNearPlayer = false;

    public bool enableMovement { get { return _canMove; } set { _canMove = value; } }
    public bool isNearPlayer { get { return _isNearPlayer; } }

    void Start() {
        _agent = GetComponent<NavMeshAgent>();
    }

    private void Update() {
        _isNearPlayer = Vector3.Distance(transform.position, target.position) <= minDistanceToPlayer;
    }

    /// <summary>
    /// Moves enemy towards player
    /// </summary>
    public void Move() {
        if (Vector3.Distance(transform.position, target.position) >= minDistanceToPlayer && _canMove) {
            _agent.isStopped = false;
            _agent.destination = target.position;
        } else {
            _agent.isStopped = true;
            SmoothLookAt(target);
        }
    }

    // AUX METHODS

    /// <summary>
    /// Rotates transform to look at a desired target
    /// </summary>
    /// <param name="target"></param>
    public void SmoothLookAt(Transform target) {
        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, lookRotationSpeed * Time.deltaTime);
    }
}
