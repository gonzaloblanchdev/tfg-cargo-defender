using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    [Header("Config")]
    public float health = 100;
    public float armor = 25;

    private float _currentHealth;
    private BulletController _bullet;

    private void Start() {
        _currentHealth = health + armor;
    }

    public void TakeDamage(float damage) {
        _currentHealth -= damage;
        Debug.Log("ENEMY DAMAGED: " + _currentHealth);
        if(_currentHealth <= 0) {
            GameManager.instance.wavesManager.OnEnemyKilled();
            Destroy(gameObject);
        }
    }
}
