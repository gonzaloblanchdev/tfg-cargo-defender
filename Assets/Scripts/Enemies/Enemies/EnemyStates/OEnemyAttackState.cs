using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OEnemyAttackState : EnemyStateBase {

    [Header("Burst Config")]
    public float burstTime = 3f;
    [Header("Related States")]
    public string movementState;

    private bool _canShoot = false;
    private float _currentTimer = 0;

    private void Awake() {
        stateMachine = GetComponentInParent<StateMachineController>();
    }
    public override void StateEnter() {
        _canShoot = true;
        _currentTimer = 0;
    }

    public override void StateExit() {
        
    }

    public override void StateInput() {
        _currentTimer += Time.deltaTime;

        if (_currentTimer >= burstTime) {
            _canShoot = !_canShoot;
            _currentTimer = 0;
        }

        if (_canShoot) {
            enemyController.gun.Shoot();
        }

        enemyController.movement.SmoothLookAt(GameDataBackup.instance.playerTransform);
    }

    public override void StateLateStep() {
        
    }

    public override void StatePhysicsStep() {
        
    }

    public override void StateStep() {
        if (!enemyController.movement.isNearPlayer) {
            stateMachine.SetState(movementState);
        }
    }
}