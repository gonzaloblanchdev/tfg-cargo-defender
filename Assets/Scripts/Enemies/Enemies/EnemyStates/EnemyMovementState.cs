using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementState : EnemyStateBase {

    [Header("Related States")]
    public string attackState;

    private void Awake() {
        stateMachine = GetComponentInParent<StateMachineController>();
    }

    public override void StateEnter() {
        // Enabling movement
        enemyController.movement.enableMovement = true;
    }

    public override void StateExit() {
        
    }

    public override void StateInput() {
        // Moving the enemy
        enemyController.movement.Move();
    }

    public override void StateLateStep() {
        
    }

    public override void StatePhysicsStep() {
        
    }

    public override void StateStep() {
        if (enemyController.movement.isNearPlayer) {
            stateMachine.SetState(attackState);
        }
    }
}