using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyController : MonoBehaviour{

    #region "PUBLICS"
    // PUBLICS
    [Header("References")]
    public EnemyMotor motor;
    public EnemyShootingSystem shootingSystem;
    public StateMachineController stateMachine;
    public Animator animator;
    public SphereCollider playerDetectionSphere;
    [Header("Enemy Stats")]
    public float enemyHealth = 100f;
    public float enemyArmor = 20f;
    [Header("Player Detection")]
    public float playerDetectionRadius = 9f;
    [Header("Covers")]
    public List<GameObject> covers;
    [Header("Overlap Config")]
    public Transform overlapCenter;
    public Vector3 overlapSize;
    public LayerMask detectionLayer;
    #endregion
    #region "PRIVATES"
    // PRIVATES
    [SerializeField]
    [Header("DEBUG")]
    private bool _enemyOnDeck;
    public float _currentHealth;
    [SerializeField]
    private bool _playerInRange;
    private bool _playerInSight;
    [SerializeField]
    private bool _isDead = false;
    #endregion
    #region "SETTERS & GETTERS"
    // SETTERS & GETTERS
    public bool getEnemyOnDeck {
        get { return _enemyOnDeck; } 
    }

    public bool getPlayerInRange {
        get { return _playerInRange; }
    }

    bool getPlayerInSight {
        get { return _playerInSight;}
    }
    #endregion

    #region"UNITY_METHODS"

    private void Start()
    {
        stateMachine.Initialize();
        _currentHealth = enemyHealth + enemyArmor;
    }

    private void Update()
    {
        CheckEnemyOnDeck();
        CheckPlayerInRange();
        stateMachine.Step();
    }

    private void FixedUpdate()
    {
        stateMachine.PhysicsStep();
    }

    private void LateUpdate()
    {
        stateMachine.LateStep();
    }
    #endregion

    /// <summary>
    /// Comprueba si el enemigo se encuentra en la cubierta del barco
    /// </summary>
    private void CheckEnemyOnDeck() {
        Collider[] collisions = Physics.OverlapBox(overlapCenter.position, overlapSize / 2f, overlapCenter.rotation, detectionLayer);

        if(collisions != null && collisions.Length > 0) {
            _enemyOnDeck = true;
        } else {
            _enemyOnDeck = false;
        }
    }

    /// <summary>
    /// Comprueba si el jugador est� a rango del enemigo
    /// </summary>
    private void CheckPlayerInRange() {
        Collider[] collisions = Physics.OverlapSphere(transform.position, playerDetectionRadius);

        if(collisions.Length > 0)
        {
            foreach (Collider collision in collisions)
            {
                if (collision.gameObject.CompareTag("Player"))
                {
                    _playerInRange = true;
                }
            }
        }
        else
        {
            _playerInRange = false;
        }
    }

        

    /// <summary>
    /// Comprueba si el jugador est� a rango del enemigo
    /// </summary>
    /// <param name="other"></param>
    //private void OnTriggerEnter(Collider other) {
    //    if (other.gameObject.CompareTag("Player")) {
    //        _playerInRange = true;
    //    }
    //}

    /// <summary>
    /// Comprueba si el jugador ha salido del rango del enemigo
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            _playerInRange = false;
        }
    }

    /// <summary>
    /// Comprueba si el jugador est� a la vista del enemigo
    /// </summary>
    private void CheckPlayerInSight() {

    }

    public void EnemyDamaged(float damage) {
        _currentHealth -= damage;
        if(_currentHealth <= 0) {
            _isDead = true;
        }
        Debug.Log(_currentHealth);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(overlapCenter.position, overlapSize);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, playerDetectionRadius);
    }
}
