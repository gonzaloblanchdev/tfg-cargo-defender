using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMotor : MonoBehaviour
{
    #region "PUBLICS"
    // PUBLICS
    [Header("References")]
    //public CharacterController controller;
    public Rigidbody rb;
    public Seeker seeker;
    [Header("Movement Config")]
    public bool movementEnabled = false;
    public float moveSpeed = 5f;
    public float rotationSpeed = 1f;
    [Header("Path Config")]
    public float updateRate;
    public float waypointChangeDistance;
    public float pathAlmostFinishedThreshold;
    public bool canUpdatePath;
    public Transform target;
    #endregion
    #region "PRIVATES"
    // PRIVATES
    private bool _pathFinished;
    [SerializeField]
    private bool _pathAlmostFinished;
    private int _currentWaypoint;
    private Path _path;
    private float _currentUpdateRate;
    private float _wayPointChangeDistanceBackUp;
    #endregion
    #region "SETTERS & GETTERS"
    // Setters & Getters
    public bool getPathFinished {
        get { return _pathFinished; }
    }

    public bool getPathAlmostFinished {
        get { return _pathAlmostFinished; }
    }
    #endregion

    private void Start() {
        seeker.StartPath(transform.position, target.position, OnPathGenerated);
        _currentUpdateRate = updateRate;
        _wayPointChangeDistanceBackUp = waypointChangeDistance;
    }

    private void FixedUpdate() {
        //Movement();
    }

    private void Update() {
        Movement();
    }

    private void LateUpdate() {
        //Movement();
        if (canUpdatePath) {
            UpdatePath();
        }
    }

    /// <summary>
    /// M�todo que se ejecuta cuando se genera un path
    /// </summary>
    /// <param name="p"></param>
    private void OnPathGenerated(Path p) {
        if (!p.error) {
            _path = p;
            _pathFinished = false;
            _pathAlmostFinished = false;
            _currentWaypoint = 0;
        } else {
            _path = null;
        }
    }

    /// <summary>
    /// M�todo que aplica el movimiento por un path
    /// </summary>
    private void Movement() {
        if(movementEnabled && _path != null) {
            // Comprobamos si casi ha terminado el camino
            // TODO: que el �ltimo waypoint llegue hasta el final
            if(_currentWaypoint == _path.vectorPath.Count - pathAlmostFinishedThreshold) {
                _pathAlmostFinished = true;
            }

            if (_pathAlmostFinished) {

                waypointChangeDistance = 0.1f;

            } else {

                waypointChangeDistance = _wayPointChangeDistanceBackUp;

            }

        // Comprobamos si hemos llegado al final del camino
        if (_currentWaypoint >= _path.vectorPath.Count) {
                _pathFinished = true;
                rb.velocity = Vector3.zero;
                return;
            }

            // Si la distancia que hay hasta el waypoint objetivo es menor
            // a la establecida para cambiar de waypoint
            if(Vector3.Distance(transform.position, _path.vectorPath[_currentWaypoint]) < waypointChangeDistance) {
                _currentWaypoint++;
            } else {
                // Continuamos movimiento
                Vector3 direction = _path.vectorPath[_currentWaypoint] - transform.position;
                rb.velocity = direction.normalized * moveSpeed;
                SmoothLookAt();
            }
        } else {
            rb.velocity = Vector3.zero;
        }
    }

    /// <summary>
    /// M�todo que actualiza el path dado un ratio de actualizaci�n
    /// </summary>
    private void UpdatePath() {
        _currentUpdateRate -= Time.deltaTime;
        if(movementEnabled && _currentUpdateRate <= 0) {
            seeker.StartPath(transform.position, target.position, OnPathGenerated);
            _currentUpdateRate = updateRate;
        }
    }

    /// <summary>
    /// M�todo que cambia el target al que se dirige el enemigo
    /// </summary>
    /// <param name="newTarget"></param>
    public void ChangeTarget(Transform newTarget) {
        target = newTarget;
        seeker.StartPath(transform.position, target.position, OnPathGenerated);
    }

    /// <summary>
    /// M�todo que orienta el enemigo hacia su direcci�n de forma suave
    /// </summary>
    private void SmoothLookAt() {
        Quaternion targetRotation = Quaternion.LookRotation(_path.vectorPath[_currentWaypoint] - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    /// <summary>
    /// TODO: Funci�n que hace que el jugador se coloque en 
    /// la posici�n de una covertura
    /// </summary>
    private void TakeCover() {

    }

    /// <summary>
    /// TODO: M�todo que hace que el enemigo se asome por la
    /// cobertura para disparar
    /// </summary>
    private void Peek() {

    }

    /// <summary>
    /// TODO: M�todo que hace que el enemigo trepe por el 
    /// lateral del barco
    /// </summary>
    private void Climb() {

    }
}
