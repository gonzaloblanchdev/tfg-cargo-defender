using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingSystem : MonoBehaviour {

    // PUBLICS
    [Header("References")]
    public GunControllerBase enemyGun;
    [Header("Shooting Config")]
    public float burstTime = 2f;
    public float burstCoolDown;

    // PRIVATES 
    private float _currentBurstTime;
    private float _currentBurstCoolDown;
    [SerializeField]
    private bool _canShoot = true;

    private void Start() {
        _currentBurstTime = burstTime;
        _currentBurstCoolDown = burstCoolDown;
    }

    /// <summary>
    /// Apunta su forward hacia target solo rotando en Y
    /// </summary>
    /// <param name="target"></param>
    public void Aim(Transform target) {
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        enemyGun.gameObject.transform.LookAt(new Vector3(target.position.x, enemyGun.gameObject.transform.position.y, target.position.z));
    }

    public void Shoot() {
        if (_canShoot)
        {
            _currentBurstTime -= Time.deltaTime;
            enemyGun.BaseShoot();

            if (_currentBurstTime <= 0) {
                _canShoot = false;
            }
        } else {
            _currentBurstCoolDown -= Time.deltaTime; 
            if(_currentBurstCoolDown <= 0) {
                _canShoot = true;
                _currentBurstTime = burstTime;
                _currentBurstCoolDown = burstCoolDown;
            }
        }
        
    }
}
