using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPistol : GunControllerBase
{
    private void Start() {
        ConvertFireRateToSeconds();
        currentMags = totalMags;
    }

    void Update() {

        if (enableTimer) {
            RateOfFireTimer();
        }
    }
}
