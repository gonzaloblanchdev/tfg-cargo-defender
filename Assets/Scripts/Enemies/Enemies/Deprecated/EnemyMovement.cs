using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyMovement : MonoBehaviour {

    [Header("References")]
    public Rigidbody rb;
    public Seeker seeker;
    public UnityEvent patrolIndexUpdate;
    [Header("Configuration")]
    public bool canUpdatePath = false;
    public bool movementEnabled = false;
    public bool pathFinished = false;
    public bool pathAlmostFinished = false;
    public int pathAlmostFinishedThreshold = 10;
    public float distanceToChangeWayPoint;
    public float speed;
    public float rotationSpeed;
    public float updateRate;
    public Transform target;

    // PRIVATES
    private int _currentWayPoint;
    private Path _path;
    private float currentUpdateRate;

    void Start() {
        seeker.StartPath(transform.position, target.position, OnPathGenerated);
        currentUpdateRate = updateRate;
        Debug.Log(target.position);
    }

    private void LateUpdate() {
        Movement();
        if (canUpdatePath)
        {
            UpdatePath();
        }
        
    }

    private void OnPathGenerated(Path p) {
        if (!p.error) {
            _path = p;
            pathFinished = false;
            pathAlmostFinished = false;
            patrolIndexUpdate.Invoke();
            _currentWayPoint = 0;
        } else {
            _path = null;
        }
    }

    private void Movement() {
        if(movementEnabled && _path != null) {
            // Comprobamos si casi ha terminado el camino para cambiar de destino
            if(_currentWayPoint == _path.vectorPath.Count - pathAlmostFinishedThreshold)
            {
                pathAlmostFinished = true; 
            }
            // Si hemos llegado al final del camino
            if (_currentWayPoint >= _path.vectorPath.Count) {
                pathFinished = true;
                rb.velocity = Vector3.zero;
                return;
            }

            // Si la distancia que hay hasta el waypoint objetivo es menor 
            // a la establecida para cambiar de waypoint
            if(Vector3.Distance(transform.position, _path.vectorPath[_currentWayPoint]) < distanceToChangeWayPoint) {
                _currentWayPoint++;
            } else {
                // Continuamos movimiento
                Vector3 direction = _path.vectorPath[_currentWayPoint] - transform.position;
                rb.velocity = direction.normalized * speed;
                //transform.LookAt(_path.vectorPath[_currentWayPoint]);
                SmoothLookAt();
            }
        } else {
            rb.velocity = Vector3.zero;
        }
    }

    public void SmoothLookAt() {
        Quaternion targetRotation = Quaternion.LookRotation(_path.vectorPath[_currentWayPoint] - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
    }

    private void UpdatePath() {
        currentUpdateRate -= Time.deltaTime;
        if(movementEnabled && currentUpdateRate <= 0) {
            seeker.StartPath(transform.position, target.position, OnPathGenerated);
            currentUpdateRate = updateRate;
        }
    }

    public void ChangeTarget(Transform newTarget){
        target = newTarget;
        seeker.StartPath(transform.position, target.position, OnPathGenerated);
    }
}
