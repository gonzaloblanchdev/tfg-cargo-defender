using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IABoatController : MonoBehaviour {

    public IABoatMovement movement;
    public StateMachineController stateMachine;
    public GameObject[] enemiesOnBoat;
    [Header("DEBUG")]
    public bool destroyAutomatically = false;
    public float destroyTime = 50f;

    private float _currentTimer = 0;

    void Start() {
        stateMachine.Initialize();
    }
    void Update() {
        stateMachine.Step();
        DestroyTimer();
    }

    private void FixedUpdate() {
        stateMachine.PhysicsStep();
    }

    private void LateUpdate() {
        stateMachine.LateStep();
    }

    // DEBUG ONLY
    private void DestroyTimer() {
        _currentTimer += Time.deltaTime;
        if(_currentTimer >= destroyTime) {
            Destroy(gameObject);
        }
    }
}
