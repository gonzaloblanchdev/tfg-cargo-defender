using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IABoatMovement : MonoBehaviour {

    public Transform[] points;
    public float distanceToChangeTarget = 5f;
    public bool loopMovement = false;

    private NavMeshAgent _agent;
    private bool _canMove = true;
    private int _pointIndex = 0;
    private bool _hasReachedDestination = false;

    public bool enableMovement {  get { return _canMove; } set {  _canMove = value; } }
    public bool hasReachedDestination { get { return _hasReachedDestination; } set { _hasReachedDestination = value; } }

    private void Awake() {
        _agent = GetComponent<NavMeshAgent>();
    }

    //private void Update() {
    //    MoveBoat();
    //}

    public void MoveBoat() {
        if(!_canMove) return;

        // Setting the new destination
        _agent.destination = points[_pointIndex].position;
        Debug.Log(_agent.destination);

        // If path is finished
        if(Vector3.Distance(transform.position, points[_pointIndex].position) < distanceToChangeTarget) { 
            // We go to next position on the array
            if(_pointIndex < points.Length-1) {
                _pointIndex++;
            }

            if(!loopMovement && _pointIndex == points.Length - 1 && !_agent.hasPath) {
                _agent.isStopped = true;
                _hasReachedDestination = true;

            }

            if(loopMovement && _pointIndex == points.Length - 1) {
                _pointIndex = 0;
            }
        }
    }

    public void UpdatePoints(Transform[] newPoints) {
        points = newPoints;
        _pointIndex = 0;
    }

}
