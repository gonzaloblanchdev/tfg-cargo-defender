using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropEnemiesBoatState : BoatStateBase
{
    [Header("#Drop Enemies State")]
    public float enemyDropTime = 5;
    [Header("Related States")]
    public string patrolState;

    private float _timer = 0;

    private void Awake() {
        stateMachine = GetComponentInParent<StateMachineController>();
    }

    public override void StateEnter() {
        boatController.movement.UpdatePoints(GameDataBackup.instance.dropPoints);
        boatController.movement.enableMovement = true;
        boatController.movement.loopMovement = false;
        _timer = 0;
    }

    public override void StateExit() {
        boatController.movement.hasReachedDestination = false;
        boatController.movement.enableMovement = false;
        GameManager.instance.LoadEnemiesInCargoSpawner(boatController.enemiesOnBoat);
    }

    public override void StateInput() {
        boatController.movement.MoveBoat();
    }

    public override void StateLateStep() {
        
    }

    public override void StatePhysicsStep() {
        
    }

    public override void StateStep() {
        if (boatController.movement.hasReachedDestination) {
            _timer += Time.deltaTime;
            if (_timer >= enemyDropTime) {
                stateMachine.SetState(patrolState);
            }
        }
        
    }

}