using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBoatState : BoatStateBase
{
    private void Awake() {
        stateMachine = GetComponentInParent<StateMachineController>();
    }

    public override void StateEnter() {
        boatController.movement.UpdatePoints(GameDataBackup.instance.patrolPoints);
        boatController.movement.enableMovement = true;
        boatController.movement.loopMovement = true;
        boatController.gameObject.SetActive(false);
        boatController.gameObject.SetActive(true);
    }

    public override void StateExit() {
        
    }

    public override void StateInput() {
        
    }

    public override void StateLateStep() {
        
    }

    public override void StatePhysicsStep() {
        
    }

    public override void StateStep() {
        boatController.movement.MoveBoat();
    }

}