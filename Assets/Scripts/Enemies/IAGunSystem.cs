using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAGunSystem : MonoBehaviour {

    // PUBLICS
    public Transform shootOrigin;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LayerMask playerMask;
    [Header("Gun Stats")]
    public int bulletsPerShot;
    public float damage;
    public float timebetweenShooting;
    public float spread;
    public float range;
    public float reloadTime;
    public float timeBetweenShots;
    public int magazineSize;
    public bool allowAutomatic;
    [Header("Particles")]
    public ParticleSystem gunMuzzlePS;

    // PRIVATES
    private int bulletsLeft;
    private int bulletsShot;
    private bool shooting;
    private bool readyToShoot;
    private bool reloading;

    private void Start() {
        bulletsLeft = magazineSize;
        readyToShoot = true;
        bulletsShot = bulletsPerShot;
    }

    public void Shoot() {

        if (reloading || !readyToShoot) return;

        readyToShoot = false;

        // Spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        // Ading spread to shooting direction
        Vector3 direction = shootOrigin.transform.forward + new Vector3(x, y, 0);

        // Raycast shoot
        if(Physics.Raycast(shootOrigin.position, direction, out rayHit, range, playerMask)) {
            Debug.Log(rayHit.collider.name);

            if (rayHit.collider.CompareTag("Player")) {
                // TODO: quitar vida a jugador
                Debug.Log("PLAYER HIT");
            }
        }

        // Particles
        gunMuzzlePS.Play();

        bulletsLeft--;
        bulletsShot--;

        Debug.Log(bulletsLeft);

        Invoke("ResetShot", timebetweenShooting);

        if (bulletsShot > 0 && bulletsLeft > 0) {
            Invoke("Shoot", timeBetweenShots);
        }

        if(bulletsLeft == 0) {
            Reload();
        }

        AudioManager.instance.PlaySound("RifleShot");
    }

    private void ResetShot() {
        readyToShoot = true;
    }

    private void Reload() {
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }

    private void ReloadFinished() {
        bulletsLeft = magazineSize;
        bulletsShot = bulletsPerShot;
        reloading = false;
    }

}
