using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable {

    public float maxHealth {  get; set; }
    public float currentHealth { get; set; }

    public void ReceiveDamage(float damageAmountk);
    public void Die();

}
