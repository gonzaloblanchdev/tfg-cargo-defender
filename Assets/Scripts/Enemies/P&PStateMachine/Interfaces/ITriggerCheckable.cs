using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerCheckable {
    
    bool isAggroed { get; set; }
    bool isWithinStrikingDistance { get; set; }


    public void SetAggroStatus(bool isAggroed);
    void SetIsWithinStrikingDistanceStatus(bool isWithinStrikingDistance);
}
