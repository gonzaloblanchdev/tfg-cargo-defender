using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamagable, ITriggerCheckable {

    // INTERFACES
    public float maxHealth { get; set; } = 100f;
    public float currentHealth { get; set; }
    public bool isAggroed { get; set; }
    public bool isWithinStrikingDistance { get; set; }

    #region "State Machine Variables"

    public EnemyStateMachine stateMachine { get; set; }

    public EnemyIdleState idleState { get; set; }
    public EnemyChaseState chaseState { get; set; }
    public EnemyAttackState attackState { get; set; }

    #endregion

    #region "Scriptable Objects Variables"

    [SerializeField] private EnemyIdleSOBase enemyIdle;
    [SerializeField] private EnemyChaseSOBase enemyChase;
    [SerializeField] private EnemyAttackSOBase enemyAttack;

    // Instances in order to not change all at once
    public EnemyIdleSOBase enemyIdleInstance { get; set; }
    public EnemyChaseSOBase enemyChaseInstance { get; set; }
    public EnemyAttackSOBase enemyAttackInstance { get; set; }

    #endregion

    #region "UNITY METHODS"

    private void Awake() {
        CreateSOStatesInstances();
        CreateStateMachineAndStates();
    }

    private void Start() {
        currentHealth = maxHealth;
        InitializeStateInstances();
        // DEFAULT STATE: idleState
        stateMachine.Initialize(idleState);
    }

    private void Update() {
        stateMachine.currentEnemyState.FrameUpdate();
    }

    private void FixedUpdate() {
        stateMachine.currentEnemyState.PhysiscsUpdate();
    }

    #endregion

    private void AnimationTriggerEvent(AnimationTriggerType triggerType) {
        stateMachine.currentEnemyState.AnimationTriggerEvent(triggerType);
    }

    #region "Initialization Methods"

    private void CreateSOStatesInstances() {
        enemyIdleInstance = Instantiate(enemyIdle);
        enemyChaseInstance = Instantiate(enemyChase);
        enemyAttackInstance = Instantiate(enemyAttack);
    }

    private void CreateStateMachineAndStates() {
        stateMachine = new EnemyStateMachine();

        idleState = new EnemyIdleState(this, stateMachine);
        chaseState = new EnemyChaseState(this, stateMachine);
        attackState = new EnemyAttackState(this, stateMachine);
    }

    private void InitializeStateInstances() {
        enemyIdleInstance.Initialize(gameObject, this);
        enemyChaseInstance.Initialize(gameObject, this);
        enemyAttackInstance.Initialize(gameObject, this);
    }

    #endregion

    #region "IDamagable Implementation"

    public void Die() {
        throw new System.NotImplementedException();
    }

    public void ReceiveDamage(float damageAmountk) {
        throw new System.NotImplementedException();
    }

    #endregion

    #region "ITriggerCheckable Implementation"

    public void SetAggroStatus(bool isAggroed) {
        throw new System.NotImplementedException();
    }

    public void SetIsWithinStrikingDistanceStatus(bool isWithinStrikingDistance) {
        throw new System.NotImplementedException();
    }

    public enum AnimationTriggerType {
        EnemyDamage,
        PlayFootStepsSound
    }

    #endregion

    
}
