using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Este script se encarga de gestionar el Input generado por el Input System
/// </summary>

public class InputManager : MonoBehaviour
{
    // REFERENCES
    private PlayerInput playerInput;
    public PlayerInput.OnFootActions onFoot;
    public PlayerInput.GunsActions guns;
    public PlayerInput.UIActions ui;
    private PlayerMotor motor;
    private PlayerLook look;
    private GunInventoryController gunInventory;
    private Animator animator;
    public PauseMenuController pauseMenu;

    // PRIVATES
    [SerializeField]
    private bool pauseMenuIsOpen = false;

    private void Awake() {
        // Creamos un player Input
        playerInput = new PlayerInput();
        // Asignamos los Action Maps
        onFoot  = playerInput.OnFoot;
        guns    = playerInput.Guns;
        ui      = playerInput.UI;

        // Obtenemos referencias necesarias
        motor           = GetComponent<PlayerMotor>();
        look            = GetComponent<PlayerLook>();
        gunInventory    = GetComponentInChildren<GunInventoryController>(); 
        animator        = GetComponent<Animator>();
        //pauseMenu       = GetComponentInChildren<PauseMenuController>();

        // Asignamos eventos a Inputs
        onFoot.Jump.performed       += ctx => motor.Jump();
        onFoot.Crouch.performed += ctx => motor.Crouch();
        guns.ChangeGun.performed    += ctx => gunInventory.UpdateGunIndex(guns.ChangeGun.ReadValue<Vector2>());
        guns.ThrowGun.performed     += ctx => gunInventory.RemoveCurrentGun();
        ui.PauseMenu.performed      += ctx => OpenClosePauseMenu();
    }

    private void Start() {
        pauseMenu.gameObject.SetActive(false);
    }

    // Activamos los inputs en el Enable
    private void OnEnable() {
        onFoot.Enable();
        guns.Enable();
        ui.Enable();
    }

    // Desactivamos los inputs en el Disable
    private void OnDisable() {
        onFoot.Disable();
        guns.Disable();
        ui.Disable();
    }

    private void FixedUpdate() {

        // Usamos la funci�n de PlayerMotor para movernos y como input, accedemos a 
        // lo configuirada previamente en el Input System, usando la funci�n ReadValue
        // Y pas�ndole el tipo de dato que queremos leer.
        motor.CalculateMovement(onFoot.Movement.ReadValue<Vector2>());

        // Comprobamos si se est� presionando el control de sprint y 
        // cambiamos el estado de la variable isSprinting
        if (onFoot.Sprint.IsPressed()) {
            motor.isSprinting = true;
        } else {
            motor.isSprinting = false;
        }

        // Ejecutamos la funci�n de sprint
        motor.Sprint();

        // Actualizamos las animaciones
        if(onFoot.Movement.ReadValue<Vector2>().magnitude != 0) {
            animator.SetBool("isWalking", true);
        } else {
            animator.SetBool("isWalking", false);
        }

        animator.SetBool("isRunning", motor.isSprinting);
    }

    private void LateUpdate() {
        look.CalculateLook(onFoot.Look.ReadValue<Vector2>());

    }

    // FUNCIONES DE MEN� DE PAUSA

    /// <summary>
    /// Abre o cierra el men� de pausa
    /// </summary>
    private void OpenClosePauseMenu() {
        
        // Actualizamos el estado de visibilidad del men� de pausa
        pauseMenuIsOpen = !pauseMenuIsOpen;
        pauseMenu.gameObject.SetActive(pauseMenuIsOpen);

        // Paramos o reanudamos el juego
        if (pauseMenuIsOpen) {
            GameManager.instance.PauseGame();
        } else {
            GameManager.instance.ResumeGame();
        }
    }
}
