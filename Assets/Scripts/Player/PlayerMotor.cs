using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Este script se encarga del movimiento del player.
/// </summary>

public class PlayerMotor : MonoBehaviour
{
    // PUBLICS
    public float speed = 5f;
    public float speedMultiplierOnSprint = 2f;
    public float gravity = -9.8f;
    public float jumpForce = 3f;
    [HideInInspector]
    public bool isSprinting = false;
    [HideInInspector]
    public bool crouching = false;

    // PRIVATES
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool isGrounded;
    private float originalSpeed;
    private float sprintSpeed;
    private float crouchTimer;
    private bool lerpCrouch = false;


    private void Start() {
        controller = GetComponent<CharacterController>();
        originalSpeed = speed;
        sprintSpeed = speed * speedMultiplierOnSprint;
    }

    private void Update() {
        isGrounded = controller.isGrounded;

        // LERP: A la variable crouchtimer se le a�ade la variable de tiempo, que tomar� valores (0,1 0,2 0,3...)
        // La dividimos entre uno para que la variable p aumente pero no pase a 1. La variable va a ser igual a 
        // crouchtimer lo �nico que nunca pasa de uno. Por eso la divsi�n entre uno.
        crouchTimer += Time.deltaTime;
        float p = crouchTimer / 1;
        // Para que el agachado sea exponencial.
        p *= p;
        if (crouching) {
            controller.height = Mathf.Lerp(controller.height, 1, p);
        } else {
            controller.height = Mathf.Lerp(controller.height, 2, p);
        }

        if(p > 1) {
            lerpCrouch = false;
            crouchTimer = 0;
        }
    }

    /// <summary>
    /// Calcula el movimiento en funci�n de la variable input recibida
    /// </summary>
    /// <param name="input"></param>
    public void CalculateMovement(Vector2 input) {
        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        // Aplicamos el movimiento en los ejes X y Z
        controller.Move(transform.TransformDirection(moveDirection) * speed * Time.deltaTime);
        // Sumamos continuamente por que es una aceleraci�n. Y almacenamos en componente Y.
        // De esta manera dejamos toda la informaci�n del movimiento en un Vector3
        playerVelocity.y += gravity * Time.deltaTime;
        if(isGrounded && playerVelocity.y < 0) {
            // Cuando estamos en el suelo, limitamos la gravedad para que no se sume indefinidamente
            playerVelocity.y = -2f;
        }
        // Aplicamos la gravedad
        controller.Move(playerVelocity * Time.deltaTime);
    }

    public void Jump() {
        if (isGrounded) {
            playerVelocity.y = Mathf.Sqrt(-jumpForce * gravity);
        }
    }

    public void Sprint() {
        if (isSprinting) {
            speed = sprintSpeed;
        } else {
            speed = originalSpeed;
        }
    }

    public void Crouch() {
        crouching = !crouching;
        crouchTimer = 0;
        lerpCrouch = true;
    }
}
