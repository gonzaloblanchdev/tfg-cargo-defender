using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script encargado de la interacci�n del player con los
/// objetos interactuables
/// </summary>

public class PlayerInteract : MonoBehaviour {

    private Camera _cam;
    [SerializeField]
    private float rayDistance = 3f;
    [SerializeField]
    private LayerMask interactableMask;
    private PlayerUI playerUI;
    private InputManager inputManager;

    void Start() {
        // Obtengo la c�mara del PlayerLook por que ya tiene la referencia
        _cam = GetComponent<PlayerLook>().cam;
        playerUI = GetComponent<PlayerUI>();
        inputManager = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update() {
        // Actualizamos el prompt para que no aparezca nada en pantalla
        playerUI.UpdatePromptText(string.Empty);
        // Creamos un rayo encargado de detectar los interactuables
        Ray ray = new Ray(_cam.transform.position, _cam.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * rayDistance);
        // Variable donde se almacena la informaci�n del rayo
        RaycastHit hitInfo;

        // Si detectamos colisi�n con un interactuable
        if(Physics.Raycast(ray, out hitInfo, rayDistance, interactableMask)) {
            // y tiene el componente interactable
            if(hitInfo.collider.GetComponent<Interactable>() != null) {
                // Guardamos el objeto interactable
                Interactable interactable = hitInfo.collider.GetComponent<Interactable>();
                // Actualizamos el texto del prompt en la UI
                playerUI.UpdatePromptText(interactable.promptMessage);
                // Si se presiona el control de interactuar
                if (inputManager.onFoot.Interact.triggered) {
                    // Llamamos a la funci�n que se encarga de realizar la interacci�n
                    interactable.BaseInteract();
                }
            }
        }

    }
}
