using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Script encargado de gestionar los elementos de la UI
/// </summary>

public class PlayerUI : MonoBehaviour {

    [SerializeField]
    private TextMeshProUGUI promptText;

    public void UpdatePromptText(string promptMessage) {
        promptText.text = promptMessage;
    }
}
