using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Camera cam;
    private float xRotation = 0f;

    public float xSensitivity = 30f;
    public float ySensitivity = 30f;

    public void CalculateLook(Vector2 input) {
        float mouseX = input.x;
        float mouseY = input.y;
        // Calculamos la rotaci�n de la c�mara para mirar arriba o abajo
        xRotation -= (mouseY * Time.deltaTime) * ySensitivity;
        xRotation = Mathf.Clamp(xRotation, -80f, 80f);
        // Lo aplicamos al transform de la c�mara
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        // Rotamos la camara del jugador para mirar a la izq drcha
        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * xSensitivity);
    }

    public void SetLookSensitivity(float sensitivity) {
        xSensitivity = sensitivity;
        ySensitivity = sensitivity;
    }
}
