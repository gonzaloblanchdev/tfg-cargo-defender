using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoEnemySpawner : MonoBehaviour {

    public float timeBetweenSpawn = 3f;

    public GameObject[] enemiesToSpawn;

    private float _timer = 0f;
    private bool _canSpawn = true;
    private int _enemyIndex;
    private bool spawnerEnabled = false;

    private void Start() {
        spawnerEnabled = true;
    }

    private void Update() {
        if (GameManager.instance.wavesManager.canSpawnBoat && spawnerEnabled && _canSpawn && _enemyIndex < enemiesToSpawn.Length) {
            SpawnEnemies();
            GameManager.instance.wavesManager.OnEnemySpawned();
        }

        Timer();
    }

    public void SpawnEnemies() {
        _canSpawn = false;

        if(_enemyIndex == enemiesToSpawn.Length) {
            spawnerEnabled = false;
        }

        // Spawneamos todos los enemigos
        GameObject enemy = Instantiate(enemiesToSpawn[_enemyIndex], transform.position, Quaternion.identity);
        IAEnemyController enemyController = enemy.GetComponent<IAEnemyController>();
        enemyController.InitializeEnemy();

        _enemyIndex++;

    }

    public void ResetSpawner() {
        spawnerEnabled = true;
        _enemyIndex = 0;
    }

    private void Timer() {
        _timer += Time.deltaTime;

        if(_timer >= timeBetweenSpawn) {
            _canSpawn = true;
            _timer = 0f;
        }
    }

}
