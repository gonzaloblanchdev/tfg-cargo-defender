using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatSpawner : MonoBehaviour
{
    public GameObject[] boats;
    public float timeBetweenSpawn = 10f;

    private float _timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        _timer = timeBetweenSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        Timer();
    }

    private void SpawnBoat(int boatIndex) {
        Instantiate(boats[boatIndex], transform.position, transform.rotation);
    }

    private void Timer() {
        _timer += Time.deltaTime;

        if(_timer > timeBetweenSpawn) {
            // TODO: Selecci�n de barco a spawnear
            if (GameManager.instance.wavesManager.canSpawnBoat) {
                SpawnBoat(0);
            }
            _timer = 0;
        }
    }
}
