using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Scriptable Objects/RecoilData")]
public class RecoilData : ScriptableObject
{
    public float recoilX;
    public float recoilY;
    public float recoilZ;
    public float snapiness;
    public float returnSpeed;
}
